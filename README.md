# hawksoft.trafficLights package

- [hawksoft.trafficLights package](#hawksofttrafficlights-package)
  - [Installation:](#installation)
  - [usage:](#usage)

Knowledge base using pyknow for controlling traffic lights.

## Installation:

```
pip install hawksoft.trafficLights
```
## usage:

To test the knowledge library, type command as follows:
```
trafficLights
```
